#!/bin/perl

### a simple perl script I slapped together to remove all windows-reserved
### characters from all the file names in a given directory so these files
### can sync Linux -> Windows

use strict;
use warnings;

my $v = shift; # directory arg
die("no dir given") unless defined($v);

opendir my $dir, $v or die "Cannot open directory: $!";
my @files = readdir $dir;
closedir $dir;

# for each file, remove a set of common characters reserved by windows (? - \ : ' " | *)
for my $f (@files) {
	my $f2 = $f;
	$f2 =~ s/\?|\-|\\|\:|\'|\"|\||\*/\_/g; # replace with _
	if ($f ne $f2) {
		print $f." ||| ".$f2."\n";
		die("Failed to rename $f to $f2") unless rename($v."/".$f, $v."/".$f2);
	}
	
	
}
